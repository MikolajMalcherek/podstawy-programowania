#ifndef PLIKI_PGM_H
#define PLIKI_PGM_H

#include"struktury.h"

int czytaj_pgm(FILE *, t_obraz *);
int zapisz_pgm(FILE *, t_obraz *);

#endif
