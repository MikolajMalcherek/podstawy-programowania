#ifndef STRUKTURY2_H
#define STRUKTURY2_H

typedef struct {
    int jakie_p; /*jakie p(aby ustalic rozmiary tabeli)*/
    int wymx;
    int wymy;
    int szarosci;
    void *wielkosc_obrazu;
    int kolor;
    int czy_kolor;
    char *n_pliku;
    int zmienna_p; /*zmienna do filtrow, aby dla operacji na kolorach byla rowna 1, a dla operacji na wszystkihc pikselach byla rowna 3(chodzi o ilosc razy wykonanych petli w filtrach)*/
    int *po_konwersji;
    int konwertowanie;
    char *nazwa;
} t_obraz;


typedef struct {
    FILE *plik_we;
    FILE *plik_wy;
    int argc;
    char **argv;
    int negatyw;
    int konturowanie;
    int progowanie;
    int wyswietlenie;
    int w_progu;
    float w_gamma;
    int rozciaganie;
    int gamma;
    int w_progukontur;
    int polprogczerni;
    int rozmywaniepionowe;
} w_opcje;



#endif
