/*
 * File:    stack.h
 * Author:  zentut.com
 * Purpose: linked stack header file
 */
#ifndef LINKEDSTACK_H_INCLUDED
#define LINKEDSTACK_H_INCLUDED
 

struct elem* push(struct elem *lista,int elem);
struct elem* pop(struct elem *lista,int *elem);
void init(struct elem* lista);
void display(struct elem* lista);
void wyswietlszczyt(struct elem* lista);
struct elem* clear(struct elem* lista, int *element);
struct elem* reverse(struct elem* lista);
int funkcjadoduplikowania(struct elem* lista);
#endif // LINKEDSTACK_H_INCLUDED


