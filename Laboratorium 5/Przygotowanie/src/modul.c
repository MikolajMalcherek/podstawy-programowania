#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

#include "naglowekp.h"

#define MAX 5120       /* Maksymalny rozmiar wczytywanego obrazu */
#define DL_LINII 1024      /* Dlugosc buforow pomocniczych */
int obraz[MAX][MAX];
int obraz_pgm[MAX][MAX];
int wymx,wymy,odcieni;
FILE *plik;
char nazwa[100];
float wartprog;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*Funkcja czytaj*/

int czytaj(FILE *plik_we,int obraz_pgm[][MAX],int *wymx,int *wymy, int *szarosci) {
  char buf[DL_LINII];      /* bufor pomocniczy do czytania naglowka i komentarzy */
  int znak;                /* zmienna pomocnicza do czytania komentarzy */
  int koniec=0;            /* czy napotkano koniec danych w pliku */
  int i,j;

  /*Sprawdzenie czy podano prawidlowy uchwyt pliku */
  if (plik_we==NULL) {
    fprintf(stderr,"Blad: Nie podano uchwytu do pliku\n");
    return(0);
  }

  /* Sprawdzenie "numeru magicznego" - powinien być P2 */
  if (fgets(buf,DL_LINII,plik_we)==NULL)   /* Wczytanie pierwszej linii pliku do bufora */
    koniec=1;                              /* Nie udalo sie? Koniec danych! */

  if ( (buf[0]!='P') || (buf[1]!='2') || koniec) {  /* Czy jest magiczne "P2"? */
    fprintf(stderr,"Blad: To nie jest plik PGM\n");
    return(0);
  }

  /* Pominiecie komentarzy */
  do {
    if ((znak=fgetc(plik_we))=='#') {         /* Czy linia rozpoczyna sie od znaku '#'? */
      if (fgets(buf,DL_LINII,plik_we)==NULL)  /* Przeczytaj ja do bufora                */
	koniec=1;                   /* Zapamietaj ewentualny koniec danych */
    }  else {
      ungetc(znak,plik_we);                   /* Gdy przeczytany znak z poczatku linii */
    }                                         /* nie jest '#' zwroc go                 */
  } while (znak=='#' && !koniec);   /* Powtarzaj dopoki sa linie komentarza */
                                    /* i nie nastapil koniec danych         */

  /* Pobranie wymiarow obrazu i liczby odcieni szarosci */
  if (fscanf(plik_we,"%d %d %d",wymx,wymy,szarosci)!=3) {
    fprintf(stderr,"Blad: Brak wymiarow obrazu lub liczby stopni szarosci\n");
    return(0);
  }
  /* Pobranie obrazu i zapisanie w tablicy obraz_pgm*/
  for (i=0;i<*wymy;i++) {
    for (j=0;j<*wymx;j++) {
      if (fscanf(plik_we,"%d",&(obraz_pgm[i][j]))!=1) {
	fprintf(stderr,"Blad: Niewlasciwe wymiary obrazu\n");
	return(0);
      }
    }
  }
  return *wymx**wymy;   /* Czytanie zakonczone sukcesem    */
}                       /* Zwroc liczbe wczytanych pikseli */


/* Wyswietlenie obrazu o zadanej nazwie za pomoca programu "display"   */
void wyswietl(char *n_pliku) {
  char polecenie[DL_LINII];      /* bufor pomocniczy do zestawienia polecenia */

  strcpy(polecenie,"display ");  /* konstrukcja polecenia postaci */
  strcat(polecenie,n_pliku);     /* display "nazwa_pliku" &       */
  strcat(polecenie," &");
  printf("%s\n",polecenie);      /* wydruk kontrolny polecenia */
  system(polecenie);             /* wykonanie polecenia        */
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*FUNKCJA ZAPIS*/

int zapis(FILE *plik_wy,int obraz_pgm[][MAX],int *wymx,int *wymy, int *szarosci){
int nazwa;
  int i,j;
  if (plik_wy==NULL) 
    {
      fprintf(stderr,"Blad: Nie podano uchwytu do pliku\n");
      return(0);
    }
  
  fprintf(plik_wy,"P2\n%d  %d\n%d\n" ,*wymx, *wymy, *szarosci);        
/*Wpisywanie danych*/
  for (i=0;i<*wymy;i++){
    for (j=0;j<*wymx;j++) {
      fprintf(plik_wy,"%d\t",(obraz_pgm[i][j])); 
    }
    fprintf(plik_wy,"\n");
  }
  return *wymx**wymy;  /* Czytanie zakonczone sukcesem    */
}   

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*NEGATYW*/

int negatyw(int obraz_pgm[][MAX],int *wymx,int *wymy, int *szarosci){
int i,j;
for( i=0 ; i<*wymy ; i++){
  for( j=0 ; j<*wymx ; j++){
    obraz_pgm[i][j] = (*szarosci - obraz_pgm[i][j]);
    }/*for wymx*/
}/*for wymy*/
}/*NEGATYW*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*PROGOWANIE*/

int progowanie( int obraz_pgm[][MAX], int *wymx,int *wymy, int *szarosci){
printf("Podaj wartosc procentowa progu:");
scanf("%f", &wartprog);
int i,j;
int PROG;
if(wartprog>=0){
PROG = (*szarosci * (wartprog/100));
for( i=0 ; i<*wymy ; i++){
  for( j=0 ; j<*wymx ; j++){
    if(obraz_pgm[i][j] > PROG)
      obraz_pgm[i][j]=*szarosci;
    else 
    {
      obraz_pgm[i][j]=0;
    }
  }/*zamyka petle wew*/
}/*zamyka petle zew*/
}/*zamyka ifa*/
else
{
  printf("Bledna wartosc progu.\n");
}
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*KOREKCJA GAMMA*/

int korekcjagamma( int obraz_pgm[][MAX], int *wymx,int *wymy, int *szarosci){
float wartgamma;
  printf("Podaj wartosc gamma:");
  scanf("%f", &wartgamma);
  int i,j;
  for( i=0 ; i<*wymy ; i++){
    for( j=0 ; j<*wymx ; j++){
      obraz_pgm[i][j]=(pow((double)obraz_pgm[i][j]/ *szarosci, 1/wartgamma)) * *szarosci;
    }/*petla wew*/
  }/*zamyka petle zew*/
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*FUNKCJA KONTUROWANIE*/

int konturowanie(int obraz_pgm[][MAX], int *wymx,int *wymy, int *szarosci){
  int PROG=50;
  wartprog=(*szarosci * (wartprog/100));
  int i,j;
  for( i=0 ; i<*wymy ; i++){
    for( j=0 ; j<*wymx ; j++){
      obraz_pgm[i][j]=abs(obraz_pgm[i+1][j]-obraz_pgm[i][j]) + abs(obraz_pgm[i][j+1]-obraz_pgm[i][j]);
    if(obraz_pgm[i][j] > PROG)
      obraz_pgm[i][j]=*szarosci;
    else 
    {
      obraz_pgm[i][j]=0;
    }
    }
  }
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*FUNKCJA ROZCIAGANIA HISTOGRAMU*/

int rozciaganie(int obraz_pgm[][MAX], int *wymx,int *wymy, int *szarosci){
  int i,j;
  int maxszar,minszar;
        maxszar=0;
        minszar=*szarosci;
        for( i=0 ; i<*wymy ; i++){
          for( j=0 ; j<*wymx ; j++){
            if(maxszar<obraz_pgm[i][j]){
              maxszar=obraz_pgm[i][j];}
          }/*petlawew*/
        }/*petlazew*/
        for( i=0 ; i<*wymy ; i++){
          for( j=0 ; j<*wymx ; j++){
            if(minszar>obraz_pgm[i][j]){
              minszar=obraz_pgm[i][j];}
          }/*petlawew*/
        }/*petlazew*/
         for( i=0 ; i<*wymy ; i++){
         for( j=0 ; j<*wymx ; j++){
            obraz_pgm[i][j]=((obraz_pgm[i][j] - minszar)* (*szarosci)/(maxszar - minszar));
          }/*petlawew*/
        }/*petlazew*/
        printf("%d, %d\n",maxszar,minszar);
}
