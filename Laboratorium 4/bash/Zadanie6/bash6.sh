#!/bin/bash
SAVEIFS=$IFS
IFS=$(echo -en "\n\b") #kopiuje pliki z katalogu rowniez te ze spacja.
for f in /home/malcher9/podstawy-programowania/bash/Zadanie6/*.JPEG
do
  cp "$f" /home/malcher9/podstawy-programowania/bash/Zadanie6/folderzedytowanymi
done

for f in /home/malcher9/podstawy-programowania/bash/Zadanie6/*.PNG
do
  cp "$f" /home/malcher9/podstawy-programowania/bash/Zadanie6/folderzedytowanymi
done

for f in /home/malcher9/podstawy-programowania/bash/Zadanie6/*.JPG
do
  cp "$f" /home/malcher9/podstawy-programowania/bash/Zadanie6/folderzedytowanymi
done

IFS=$SAVEIFS


for f in /home/malcher9/podstawy-programowania/bash/Zadanie6/folderzedytowanymi/*
do
convert "$f" -resize $1x$2 "$f"
done


for f in /home/malcher9/podstawy-programowania/bash/Zadanie6/folderzedytowanymi/*
do
  mv "$f" "${f// /_}" #zmienia znak spacji na _
done

for f in archiwum/*
do
 case $f in
 
  *JPEG*) #gdy JPEG
  mv "$f" `echo $f | sed s/JPEG/jpeg/`;;

  *PNG*)  #gdy PNG
  mv "$f" `echo $f | sed s/PNG/png/`;;
  
  *JPG*) #gdy JPG
  mv "$f" `echo $f | sed s/JPG/jpg/`;;

 esac
done
tar cf archiwum.tar /home/malcher9/podstawy-programowania/bash/Zadanie6/folderzedytowanymi/*


