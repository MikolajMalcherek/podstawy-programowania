#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

#include "naglowek.h"
#include "struktury2.h"

#define DL_LINII 1024      /* Dlugosc buforow pomocniczych */
FILE *plik;

/********************************************************************/
/*                                                                  */
/* ALTERNATYWNA DO PRZEDSTAWIONEJ NA WYKLADZIE WERSJA OPRACOWANIA   */
/* PARAMETROW WYWOLANIA PROGRAMU UWZGLEDNIAJACA OPCJE Z PARAMETRAMI */
/* Z ODPOWIEDNIO ZAPROPONOWANYMI STRUKTURAMI DANYCH PRZEKAZUJACYMI  */
/* WCZYTANE USTAWIENIA                                              */
/*                                    COPYRIGHT (c) 2007-2020 KCiR  */
/*                                                                  */
/* Autorzy udzielaja kazdemu prawa do kopiowania tego programu      */
/* w calosci lub czesci i wykorzystania go w dowolnym celu, pod     */
/* warunkiem zacytowania zrodla                                     */
/*                                                                  */
/********************************************************************/

#define W_OK 0                   /* wartosc oznaczajaca brak bledow */
#define B_NIEPOPRAWNAOPCJA -1    /* kody bledow rozpoznawania opcji */
#define B_BRAKNAZWY   -2
#define B_BRAKWARTOSCI  -3
#define B_BRAKPLIKU   -4



/*******************************************************/
/* Funkcja inicjuje strukture wskazywana przez wybor   */
/* PRE:                                                */
/*      poprawnie zainicjowany argument wybor (!=NULL) */
/* POST:                                               */
/*      "wyzerowana" struktura wybor wybranych opcji   */
/*******************************************************/

void wyzeruj_opcje(w_opcje * wybor) {
  wybor->plik_we=NULL;
  wybor->plik_wy=NULL;
  wybor->negatyw=0;
  wybor->konturowanie=0;
  wybor->progowanie=0;
  wybor->wyswietlenie=0;
}

/************************************************************************/
/* Funkcja rozpoznaje opcje wywolania programu zapisane w tablicy argv  */
/* i zapisuje je w strukturze wybor                                     */
/* Skladnia opcji wywolania programu                                    */
/*         program {[-i nazwa] [-o nazwa] [-p liczba] [-n] [-r] [-d] }  */
/* Argumenty funkcji:                                                   */
/*         argc  -  liczba argumentow wywolania wraz z nazwa programu   */
/*         argv  -  tablica argumentow wywolania                        */
/*         wybor -  struktura z informacjami o wywolanych opcjach       */
/* PRE:                                                                 */
/*      brak                                                            */
/* POST:                                                                */
/*      funkcja otwiera odpowiednie pliki, zwraca uchwyty do nich       */
/*      w strukturze wybór, do tego ustawia na 1 pola dla opcji, ktore  */
/*	poprawnie wystapily w linii wywolania programu,                 */
/*	pola opcji nie wystepujacych w wywolaniu ustawione sa na 0;     */
/*	zwraca wartosc W_OK (0), gdy wywolanie bylo poprawne            */
/*	lub kod bledu zdefiniowany stalymi B_* (<0)                     */
/* UWAGA:                                                               */
/*      funkcja nie sprawdza istnienia i praw dostepu do plikow         */
/*      w takich przypadkach zwracane uchwyty maja wartosc NULL         */
/************************************************************************/

int przetwarzaj_opcje(int argc, char **argv, w_opcje *wybor, t_obraz *obraz) {
  int i, prog, kontur, progczerni;
  float wartoscgamma;
  char *nazwa_pliku_we, *nazwa_pliku_wy;

  obraz->czy_kolor=1; 
  obraz->kolor=0;  /*jesli chcemy dzialac na wszystkich kolorach to 1, a jesli na poszczegolnym to czy_kolor musi byc rowne 3*/
  wyzeruj_opcje(wybor);
  wybor->plik_wy=stdout;        /* na wypadek gdy nie podano opcji "-o" */
  
      
  for (i=1; i<argc; i++) {
    if (argv[i][0] != '-')  /* blad: to nie jest opcja - brak znaku "-" */
      return B_NIEPOPRAWNAOPCJA; 

    switch (argv[i][1]) {
    
    
    case 'i': {                 /* opcja z nazwa pliku wejsciowego */
      if (++i<argc) {   /* wczytujemy kolejny argument jako nazwe pliku */
	nazwa_pliku_we=argv[i];

	if (strcmp(nazwa_pliku_we,"-")==0) /* gdy nazwa jest "-"        */
	  wybor->plik_we=stdin;            /* ustwiamy wejscie na stdin */
	else                               /* otwieramy wskazany plik   */
	  wybor->plik_we=fopen(nazwa_pliku_we,"r");

      } else 
	return B_BRAKNAZWY;                   /* blad: brak nazwy pliku */
      break;
    }



    case 'o': {                 /* opcja z nazwa pliku wyjsciowego */
      if (++i<argc) {   /* wczytujemy kolejny argument jako nazwe pliku */
	nazwa_pliku_wy=argv[i];
	if (strcmp(nazwa_pliku_wy,"-")==0)/* gdy nazwa jest "-"         */
	  wybor->plik_wy=stdout;          /* ustwiamy wyjscie na stdout */
	else                              /* otwieramy wskazany plik    */
	  wybor->plik_wy=fopen(nazwa_pliku_wy,"w");
    obraz->nazwa=nazwa_pliku_wy;
      } else 
	return B_BRAKNAZWY;                   /* blad: brak nazwy pliku */
      break;
    }
    

    case 'm': {
      if (++i<argc) {
      if (obraz->jakie_p!=1){
        obraz->zmienna_p=1;
      switch(argv[i][0]){
        case 'r': {
          obraz->czy_kolor=3;
          obraz->kolor=0;              /*–––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/
          break;                       /*            kolor=0 lub kolor=1 lub kolor=2              */
        }                              /*             Przyda sie pozniej do filtrow               */
        case 'g': {                    /*               negatyw konturowanie itd.                 */
          obraz->czy_kolor=3;          /*_________________________________________________________*/
          obraz->kolor=1;              
          break;
        }
        case 'b': {
          obraz->czy_kolor=3;
          obraz->kolor=2;
          break;
        }
      }
      }/*if*/
      else{
        fprintf(stderr,"Blad: Nie mozna wykonac dzialania na kolorach dla plikow innych niz ppm.");
      }/*else */
      }
    }/*case m*/


    case 'g': {
      if (++i<argc) {
        sscanf(argv[i],"%f",&wartoscgamma);
        wybor->w_gamma=wartoscgamma;
        wybor->gamma=1;
      break;
      }
    }


    case 'p': {
      if (++i<argc) { /* wczytujemy kolejny argument jako wartosc progu */
	if (sscanf(argv[i],"%d",&prog)==1) {
	  wybor->progowanie=1;
	  wybor->w_progu=prog;
	} else
	  return B_BRAKWARTOSCI;     /* blad: niepoprawna wartosc progu */
      } else 
	return B_BRAKWARTOSCI;             /* blad: brak wartosci progu */
      break;
    }



    case 'n': {                 /* mamy wykonac negatyw */
      wybor->negatyw=1;
      break;
    }



    case 'k': {                 /* mamy wykonac konturowanie */
      wybor->konturowanie=1;
      break;
      }
  



    case 'h': {
      wybor->rozciaganie=1;
      break;
    }


    case 'd': {                 /* mamy wyswietlic obraz */
      wybor->wyswietlenie=1;
      break;
    }
    case 'v': {
      obraz->konwertowanie=1;
      break;
    } 


   case 'c': {
     if (++i<argc) { /* wczytujemy kolejny argument jako wartosc progu */
	if (sscanf(argv[i],"%d",&progczerni)==1) {
    wybor->polprogczerni=1;
    wybor->w_progu=progczerni;
  }
  else
	  return B_BRAKWARTOSCI;     /* blad: niepoprawna wartosc progu */
      } else 
	return B_BRAKWARTOSCI;             /* blad: brak wartosci progu */
      break;
   }
   case 'w': {
     wybor->rozmywaniepionowe=1;
   }
   

    default:                    /* nierozpoznana opcja */
      return B_NIEPOPRAWNAOPCJA;
    } /* koniec switch */
  } /* koniec for */

  if (wybor->plik_we!=NULL)     /* ok: wej. strumien danych zainicjowany */
    return W_OK;
  else 
    return B_BRAKPLIKU;         /* blad:  nie otwarto pliku wejsciowego  */
}


/*******************************************************/
/* Testowe wywołanie funkcji przetwarzaj_opcje         */
/* PRE:                                                */
/*      brak                                           */
/* POST:                                               */
/*      brak                                           */
/*******************************************************/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*Funkcja czytaj*/

int czytaj(FILE *plik_we,t_obraz *obraz) {
  char buf[DL_LINII];      /* bufor pomocniczy do czytania naglowka i komentarzy */
  int znak;                /* zmienna pomocnicza do czytania komentarzy */
  int koniec=0;            /* czy napotkano koniec danych w pliku */
  int i,j,k;
  /*Sprawdzenie czy podano prawidlowy uchwyt pliku */
  if (plik_we==NULL) {
    fprintf(stderr,"Blad: Nie podano uchwytu do pliku\n");
    return(0);
  }
    
  /* Sprawdzenie "numeru magicznego" - powinien być P2 lub P3 */
  if (fgets(buf,DL_LINII,plik_we)==NULL){  /* Wczytanie pierwszej linii pliku do bufora */
    koniec=1;                              /* Nie udalo sie? Koniec danych! */
  }
  
  if ( (buf[0]!='P') || (buf[1]!='2' && buf[1]!='3') || koniec) {  /* Czy jest to plik pgm Lub ppm */
    fprintf(stderr,"Blad: To nie jest plik PGM lub PPM\n");
    return(0);
  }

obraz->jakie_p=1; /*jesli dzialamy na kolorze to petla ma sie wykonac mniej razy , bo porusza się co 3 piksel oraz w przypadku pgm a w ppm bez koloru zmienna_p=3*/
if(buf[0]=='P' && buf[1]=='3'){
obraz->jakie_p = 3;
}


/*      zmienna p sluzy do        */
/*      odpowiedniej ilosci      */
/*        wykonania petli       */
obraz->zmienna_p=1;
if (obraz->jakie_p==3 && obraz->czy_kolor==1){
  obraz->zmienna_p=3;
}


  /* Pominiecie komentarzy */
  do {
    if ((znak=fgetc(plik_we))=='#') {         /* Czy linia rozpoczyna sie od znaku '#'? */
      if (fgets(buf,DL_LINII,plik_we)==NULL)  /* Przeczytaj ja do bufora                */
	koniec=1;                   /* Zapamietaj ewentualny koniec danych */
    }  else {
      ungetc(znak,plik_we);                   /* Gdy przeczytany znak z poczatku linii */
    }                                         /* nie jest '#' zwroc go                 */
  } while (znak=='#' && !koniec);   /* Powtarzaj dopoki sa linie komentarza */
                                    /* i nie nastapil koniec danych         */

  /* Pobranie wymiarow obrazu i liczby odcieni szarosci */
  if (fscanf(plik_we,"%d %d %d",&(obraz->wymx), &(obraz->wymy), &(obraz->szarosci))!=3) {
    fprintf(stderr,"Blad: Brak wymiarow obrazu lub liczby stopni szarosci\n");
    return(0);
  }

  /*alokacja pamieci na piskele obrazu*/
  obraz->wielkosc_obrazu = malloc((obraz->wymx) * (obraz->wymy) * (obraz->jakie_p) * sizeof(int)); 
  /*wskaznik na elementy tabeli*/
  int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
  /*wielkosc_obrazu jest rowna temu na co wskazuje wielkosc_obrazu powyzej*/
  wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;

  /* Pobranie obrazu i zapisanie w tablicy obraz_pgm*/
  for (i=0;i < obraz->wymy ;i++) {
    for (j=0;j < obraz->wymx * obraz->jakie_p ;j++){  /*ilosc pikseli na 1 wiersz w zaleznosci od rodzaju pliku*/
      if (fscanf(plik_we, "%d", &(wielkosc_obrazu[i][j])) != 1){ /*ilosc kolumn x 3 lub 1 + ilosc wierszy*/
	fprintf(stderr,"Blad: Niewlasciwe wymiary obrazu\n");
	return(0);
      }
   }
  }
  return obraz->wymx * obraz->wymy * obraz->jakie_p;   /* Czytanie zakonczone sukcesem    */
}                       /* Zwroc liczbe wczytanych pikseli */



/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*FUNKCJA ZAPIS*/
int zapis(FILE *plik_wy,t_obraz *obraz){
int nazwa;
  int i,j,p;
  int type=2;

  if (plik_wy==NULL) 
    {
      fprintf(stderr,"Blad: Nie podano uchwytu do pliku\n");
      return(0);
    }
  if(obraz->jakie_p==3){type=3;}
/*Wpisywanie danych*/
  fprintf(plik_wy,"P%d\n %d %d\n%d\n" ,type,obraz->wymx, obraz->wymy, obraz->szarosci);        
  int (*nowyob)[obraz->wymx];
  nowyob = (int(*)[obraz->wymx]) obraz->po_konwersji;

  int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
  wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;
  printf("konwertowanie:%d\n\n", obraz->konwertowanie);
  printf("jakie p:%d\n\n", obraz->jakie_p);
 

  if(obraz->konwertowanie==1){
  for (i=0;i<obraz->wymy;i++){
    for (j=0;j<obraz->wymx;j++){
      fprintf(plik_wy,"%d\t", nowyob[i][j]);
    }
    fprintf(plik_wy,"\n");
    }
    return obraz->wymy * obraz->wymx;
  }
else{
  for (i=0;i<obraz->wymy;i++){
    for (j=0;j<obraz->wymx * obraz->jakie_p;j++) {
      fprintf(plik_wy,"%d\t", wielkosc_obrazu[i][j]); 
    }
    fprintf(plik_wy,"\n");
  }
  } 
free(wielkosc_obrazu);
free(nowyob);


  return obraz->wymy * obraz->wymx * obraz->jakie_p;        /*    Czytanie zakonczone sukcesem    */


}