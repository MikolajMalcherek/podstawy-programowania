#ifndef operacjemat.h
#define operacjemat.h

struct elem {
    int elem;
    struct elem* nastepny;
};


struct elem* dodawanie(struct elem* lista);
struct elem* odejmowanie(struct elem* lista);
struct elem* mnozenie(struct elem* lista);
struct elem* dzielenie(struct elem* lista);
struct elem* pierw(struct elem* lista);

#endif