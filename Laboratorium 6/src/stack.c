/*
 * File:    stack.c
 * Author:  zentut.com
 * Purpose: linked stack implementation
 */
 
#include <stdio.h>
#include <stdlib.h>

struct elem {
    int elem;
    struct elem* nastepny;
};
 
/*
    init the stack
*/
void init(struct elem* lista)
{
    lista = NULL;
}
 
/*
    push an element into stack
*/
struct elem* push(struct elem* lista,int elem)
{
    struct elem* tmp = (struct elem*)malloc(sizeof(struct elem));
    if(tmp == NULL)
    {
        exit(0);
    }
    tmp->elem = elem;
    tmp->nastepny = lista;
    lista = tmp;
    return lista;
}
/*
    pop an element from the stack
*/
struct elem* pop(struct elem *lista,int *element)
{
    struct elem* tmp = lista;
    *element = lista->elem;
    lista = lista->nastepny;
    free(tmp);
    return lista;
}

/*
    display the stack content
*/
void display(struct elem* lista)
{
    struct elem *current;
    current = lista;
    if(current!= NULL)
    {
        printf("Stos: \n");
        do
        {
            printf("%d \n",current->elem);
            current = current->nastepny;
        }
        while (current!= NULL);
        printf("\n");
    }
    else
    {
        printf("Stos jest pusty.\n");
    }
}/*Display*/


/*Funkcja wyswietlszczyt*/
void wyswietlszczyt(struct elem* lista){
    if(lista!=NULL)
        printf("%d\n", lista->elem);
    else
        printf("Blad: nie ma nic do wyswietlenia.\n");
        return 0;
}


/*Funkcja clear*/
struct elem* clear(struct elem* lista, int *element){
    while(lista!=NULL){
    struct elem* tmp = lista;
    *element = lista->elem;
    lista = lista->nastepny;
        free(tmp);
    }
}





/*Funkcja reverse zamienia kolejnoscia  |
|   liczby na stosie, ale ucina ona     |
|     liczbe dodana jaka ostatnia       |
|wiec w main wywolujemy ja z push od 1 */

/*Funkcja reverse*/
struct elem* reverse(struct elem* lista){  
    int wartpoprzednia;
    wartpoprzednia=lista->nastepny->elem;
    lista->nastepny->elem=lista->elem;
    lista->elem = wartpoprzednia;
}


/*Funkcja do duplikowania*/
int funkcjadoduplikowania(struct elem* lista){
    int x;
    x=lista->elem;
    return x;
}
