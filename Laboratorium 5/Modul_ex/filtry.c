#include<stdio.h>
#include"filtry.h"

/*********************************************************************
 * Funkcja wylicza negatyw obrazu                                    *
 *				                                     *
 * \param[in] obraz struktura zawierajaca przetwarzany obraz         *
 * \param[out] obraz struktura, do ktorej zostaje zapisany negatyw   *
 * \return 0                                                         *
 *********************************************************************/

int negatyw(t_obraz * obraz) {
  
  printf("Negatywuje...\n");      /* Tu sie dzieje co trzeba */
  return 0; 
}

/*********************************************************************
 * Funkcja wylicza kontur obrazu                                     *
 *				                                     *
 * \param[in] obraz struktura zawierajaca przetwarzany obraz         *
 * \param[out] obraz struktura, do ktorej zostaje zapisany kontur    *
 * \return 0                                                         *
 *********************************************************************/

int kontur(t_obraz * obraz) {
  
  printf("Konturuje...\n");      /* Tu sie dzieje co trzeba */
  return 0; 
}
