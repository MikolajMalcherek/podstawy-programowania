
/*
 * File   : stack.c
 * Author : zentut.com
 * Purpose: linked stack program
 */
 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>


#include "stack.h"
#include "operacjemat.h"
  #define MAX 25
int main()
{
    struct elem* lista = NULL;
    int szczytstosu;
    int element;
    char napis[MAX];
    int *wart;
printf("\n");
printf("\n");
printf("         |               Kalkulator RPN ZASADY DZIALANIA:               |\n");
printf("         |          Nalezy podac dwie liczby i znak operacji            |\n");
printf("         |                jaka chcemy wykonac za nimi.                  |\n");
printf("         |Wpisujac, np. 2 2-2 kalkulator wpisze na stos liczby 0 oraz 2.|\n");
printf("\n\n");
printf("         |--------------------------------------------------------------|\n");
printf("         | q - opcja wyjscia z programu                                 |\n");
printf("         | P - wycofaj liczbe                                           |\n");
printf("         | f - wyswietl caly stos                                       |\n");
printf("         | p - wyswietl sam szczyt stosu                                |\n");
printf("         | c - wyczysc caly stos                                        |\n");
printf("         | r - odwroc dwa argumenty miejscami                           |\n");
printf("         | d - duplikuj liczbe na najwyzszym polozeniu                  |\n");
printf("         |--------------------------------------------------------------|\n");

    init(lista);
    while(1)
    {
        scanf("%s",&napis);
        if(isdigit(napis[0])){
            lista = push(lista, atoi(napis)); 
        }

            else{
                switch(napis[0]){
                    case '+':{
                        /*dodawanie*/
                        dodawanie(lista);
                        lista = pop(lista, &napis);
                        break;
                    }
                    case '-':{
                        if(isdigit(napis[1])){
                            lista=push(lista,atoi(napis));
                        }
                        else{
                        /*odejmowanie*/
                        if(lista==NULL || lista->nastepny==NULL){
                            return 0;
                        }
                        odejmowanie(lista);
                        lista = pop(lista, &napis);
                        }
                        break;
                    }
                    case '*':{
                        /*mnozenie*/
                        if(lista==NULL || lista->nastepny==NULL){
                            return 0;
                        }
                        mnozenie(lista);
                        lista = pop(lista, &napis);
                        break;
                    }
                    case '/':{
                        /*dzielenie*/
                        if(lista==NULL || lista->nastepny==NULL){
                            return 0;
                        }
                        dzielenie(lista);
                        lista = pop(lista, &napis);
                        break;
                    }
                    case 'q':{
                        return 0;
                        break;
                    }
                    case 'P':{
                        lista = pop(lista, &napis);
                        break;
                    }
                    case 'f':{
                            display(lista);
                            break;
                    }
                    case 'p':{
                        wyswietlszczyt(lista);
                        break;
                    }
                    case 'c':{
                        lista = clear(lista, &napis);
                        break;
                    }
                    case 'r':{

                        lista = reverse(lista);
                        break;
                    }
                    case 'd':{
                        szczytstosu = funkcjadoduplikowania(lista);
                        lista=push(lista, szczytstosu);
                        break;
                    }
                
                    }
                }

        }
    return 0;
}