#include<stdio.h>
#include<string.h>
#include<stdlib.h>     /*aby zmienne dynamiczne działały*/
#include<math.h>
#include"struktury2.h"
#define DL_LINII 1024  
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*NEGATYW*/

int negatyw(t_obraz *obraz){
   printf("P%d\n\n", obraz->jakie_p);
  printf("wymx: %d wymy: %d \n", obraz->wymx, obraz->wymy);
printf("wartosc zmiennej p: %d\n", obraz->zmienna_p);
int i,j,x;
int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;                                                                  /* Trzeba dodac zmienna w przetwarzaj_opcje */
                                                                                                                                                  /*  ktora bedzie zalezna od rodzaju P oraz  */                                                                                                                       /* czy bedziemy dzialac na wybranym kolorze */
for( i=0 ; i<obraz->wymy ; i++){                                                                                                                  /*      np. zmienna o nazwie czy_kolor      */
  for( j=0 ; j<obraz->wymx * obraz->zmienna_p - obraz->zmienna_p; j++){                                                                                             /*   jesli bedzie ona rowna 1, to dzialamy  */
    wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor] = ( obraz->szarosci - wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]);         /*      na wszystkich kolorach, a jesli     */
  }                                                                                                                                               /*        jest rowna 3, to na wybranym      */
  }
} 
  
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*PROGOWANIE*/

int progowanie(t_obraz *obraz, w_opcje *opcje){
int i,j;
int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu; 
printf("wartosc zmiennej_p: %d\n", obraz->zmienna_p);
printf("szarosci: %d\n", obraz->szarosci);
printf("wprogu: %d\n", opcje->w_progu);

if(opcje->w_progu<0){
  printf("Blad: nie mozna podac progu mniejszego od 0\n");
  return 0;
}
if(opcje->w_progu>=0){

for(i=0 ; i<obraz->wymy ; i++){
    for(j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
        wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=((wielkosc_obrazu[i][j* obraz->czy_kolor 
        + obraz->kolor+1]+wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor+2]+wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor+3])/3);
    }/*2 for*/
  }/*1 for*/

float PROG = ( (float) obraz->szarosci * ((float) opcje->w_progu/100));
printf("PROG: %f\n", PROG);
printf("wartosc progu: %d\n", opcje->w_progu);
for( i=0 ; i < obraz->wymy; i++){
  for( j=0 ; j < obraz->wymx * obraz->zmienna_p ; j++){
    if (wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor] > PROG)
      wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]= obraz->szarosci;
    else 
    {
       wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=0;
    }
  }/*zamyka petle wew*/
}/*zamyka petle zew*/
}/*zamyka ifa*/
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*KOREKCJA GAMMA*/
/*––––––––––––––––––––––––––––––––––––––––––*/
/*     Funckja przyjmuje wartosci gamma     */
/*          wylacznie wieksze od 0          */
/*                                          */
/*––––––––––––––––––––––––––––––––––––––––––*/


int korekcjagamma( t_obraz *obraz , w_opcje *opcje){
int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;
float x = obraz->szarosci;
printf("wartosc gamma: %f\n", opcje->w_gamma);
  if (opcje->w_gamma==0){
    printf("Zla wartosc gamma.\n");
    return 0;
  }
  int i,j;
  if(obraz->szarosci!=0){
  for( i=0 ; i<obraz->wymy ; i++){
    for( j=0 ; j<obraz->wymx * obraz->zmienna_p ; j++){
      wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=(float)(pow((double)wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]/x, (float)(1/opcje->w_gamma)))*x;
    }/*petla wew*/
  }/*zamyka petle zew*/
  }/*if*/
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*FUNKCJA KONTUROWANIE*/

int konturowanie(t_obraz *obraz, w_opcje *opcje){


int PROG = ( (float) obraz->szarosci * ( (float) opcje->w_progukontur/100));

  int i,j;
  
int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;



  for( i=0 ; i<obraz->wymy ; i++){
    for( j=0 ; j<(obraz->wymx * obraz->zmienna_p - obraz->zmienna_p); j++){
      wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=
      abs(wielkosc_obrazu[i+1][j* obraz->czy_kolor + obraz->kolor]-wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]) 
      + abs(wielkosc_obrazu[i][(j*obraz->czy_kolor  + obraz->kolor) + obraz->jakie_p]-wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]);
      if(obraz->jakie_p==3){
      wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-3]=wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-6];
      wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-2]=wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-5];
      wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-1]=wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-4];
      }/*if*/
      if(obraz->jakie_p==1){
        wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-1]=wielkosc_obrazu[i][obraz->wymx * obraz->jakie_p-2];
      }
      wielkosc_obrazu[obraz->wymy-1][j* obraz->czy_kolor + obraz->kolor]=wielkosc_obrazu[obraz->wymy-2][j* obraz->czy_kolor + obraz->kolor];
  }
    }
 
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*FUNKCJA ROZCIAGANIA HISTOGRAMU*/

int rozciaganie(t_obraz *obraz, w_opcje *opcje){
  int i,j;
  int maxszar,minszar;
  int x = obraz->szarosci;
        maxszar=0;
        minszar=obraz->szarosci;

int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;


        for( i=0 ; i< obraz->wymy ; i++){
          for( j=0 ; j< obraz->wymx * obraz->zmienna_p ; j++){
            if(maxszar<wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]){
              maxszar=wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor];}
          }/*petlawew*/
        }/*petlazew*/

        for( i=0 ; i<obraz->wymy ; i++){
          for( j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
            if(minszar>wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]){
              minszar=wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor];}
          }/*petlawew*/
        }/*petlazew*/
        int w =((float)(obraz->szarosci)/(maxszar - minszar));
        printf("w: %f\n", w);
         for( i=0 ; i<obraz->wymy ; i++){
         for( j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
           if(maxszar!=minszar && (float)(wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]) > (float)(0.5 * obraz->szarosci) && (float)(wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]) < (float)(0.8 * obraz->szarosci)){
            wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=(((float)(wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]) - (float)(minszar)) * ((float)(obraz->szarosci)/(maxszar - minszar)));
           }
          }/*petlawew*/
        }/*petlazew*/
        printf("%d, %d\n",maxszar,minszar);

}



/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*Funkcja z ppm na pgm*/


/*––––––––––––––––––––––––––––––*/
/*   Funckja bierze pod uwage   */
/*     wylacznie pliki ppm      */
/*––––––––––––––––––––––––––––––*/


int konwertowanie(t_obraz *obraz){
int i,j;
int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
  wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;
if(obraz->jakie_p==1){
  fprintf(stderr,"Blad: nie mozna wykonac konwertowania dla pliku PGM.\n");
  return 0;
}
else{
  printf("1\n");
  obraz->po_konwersji=malloc( (obraz->wymx) * (obraz->wymy) * sizeof(int) );
  int (*nowyob)[obraz->wymx];
  nowyob = (int(*)[obraz->wymx]) obraz->po_konwersji;
  printf("1\n");
  for(i=0 ; i<obraz->wymy ; i++){
    for(j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
        nowyob[i][j]=((wielkosc_obrazu[i][j*3+1]+wielkosc_obrazu[i][j*3+2]+wielkosc_obrazu[i][j*3+3])/3);
    }/*2 for*/
  }/*1 for*/
  obraz->jakie_p=1;
printf("2\n");
}



}


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*Funkcja wyswietl*/
/* Wyswietlenie obrazu o zadanej nazwie za pomoca programu "display"   */
void wyswietl(char *n_pliku) {
  char polecenie[DL_LINII];      /* bufor pomocniczy do zestawienia polecenia */

  strcpy(polecenie,"display ");  /* konstrukcja polecenia postaci */
  strcat(polecenie,n_pliku);     /* display "nazwa_pliku" &       */
  strcat(polecenie," &");
  printf("%s\n",polecenie);      /* wydruk kontrolny polecenia */
  system(polecenie);             /* wykonanie polecenia        */
}


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*Funkcja polprogowania czerni*/

int polprogczern(t_obraz *obraz, w_opcje *opcje){
int i,j;

int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;

if(opcje->w_progu>=0){
float PROG = ( (float) obraz->szarosci * ((float) opcje->w_progu/100));
for(i=0 ; i<obraz->wymy ; i++){
    for(j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
        wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=((wielkosc_obrazu[i][j* obraz->czy_kolor 
        + obraz->kolor+1]+wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor+2]+wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor+3])/3);
    }/*2 for*/
  }/*1 for*/

  
printf("%f", PROG);
  for(i=0 ; i<obraz->wymy ; i++){
    for(j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
      if (wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor] <= PROG){
        wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]= 0;
      }/*if*/
}/*for wew*/
  }/*for zew*/
}/*if*/
}/*funkcja*/



/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*Funkcja rozmywania*/

int rozmywaniepionowe(t_obraz *obraz, w_opcje *w_opcje){

int i,j;

int (*wielkosc_obrazu)[obraz->wymx * obraz->jakie_p];
wielkosc_obrazu = (int(*)[obraz->wymx * obraz->jakie_p]) obraz->wielkosc_obrazu;

for(i=0 ; i<obraz->wymy ; i++){
    for(j=0 ; j<obraz->wymx * obraz->zmienna_p; j++){
      wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor]=(float)((wielkosc_obrazu[i][j* obraz->czy_kolor 
      + obraz->kolor-1]+wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor] 
      + wielkosc_obrazu[i][j* obraz->czy_kolor + obraz->kolor+1])/3);
    }
}


}