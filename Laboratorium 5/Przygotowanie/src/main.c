#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<unistd.h>

#include "naglowekp.h"

#define MAX 5120       /* Maksymalny rozmiar wczytywanego obrazu */
#define DL_LINII 1024      /* Dlugosc buforow pomocniczych */
int obraz[MAX][MAX];
int obraz_pgm[MAX][MAX];
int wymx,wymy,odcieni;
int odczytano = 0;
FILE *plik;
char nazwa[100];
float wartprog;


int main() {

  char nropcji;
  while(1){
  printf("Proste menu:\n");
  printf("1 - Wczytaj obraz\n");
  printf("2 - Negatyw\n");  
  printf("3 - Progowanie\n");
  printf("4 - Korekcja Gamma\n");
  printf("5 - Konturowanie\n");
  printf("6 - Rozciaganie histogramu\n");
  printf("7 - Zapisz plik\n");
  printf("8 - Otworz plik\n");
  printf("9 - Zakoncz\n");
  printf("Podaj numer:\n");
  scanf("%s", &nropcji);
  switch(nropcji)
  {
    case '1':
          printf("Podaj nazwe pliku do wczytania: ");
          scanf("%s",nazwa);
          printf("\n");
          plik=fopen(nazwa,"r");

          if (plik != NULL) {       /* co spowoduje zakomentowanie tego warunku */
            odczytano = czytaj(plik,obraz,&wymx,&wymy,&odcieni);
            fclose(plik);
          if (odczytano != 0){
              printf("udalo sie wczytac %s\n", nazwa);
          }
          else{
              printf("nie udalo sie wczytac %s\n", nazwa);
          }
}
          else{
          printf("plik: %s nie istnieje,\n", nazwa);
}
break;
    case '2':
          negatyw( obraz, &wymx, &wymy, &odcieni);
        break;
    case '3':
          progowanie( obraz, &wymx, &wymy, &odcieni);
          break;
    case '4':
          korekcjagamma( obraz, &wymx, &wymy, &odcieni);
          break;
    case '5':
          konturowanie( obraz, &wymx, &wymy, &odcieni);
          break;
    case '6':
          rozciaganie( obraz, &wymx, &wymy, &odcieni);
          break;
    case '7':
          /* Wczytanie zawartosci wskazanego pliku do pamieci */
          printf("Podaj nazwe pliku:");
          scanf("%s",&nazwa);
          plik=fopen(nazwa,"w");
          zapis(plik,obraz, &wymx, &wymy, &odcieni);
          fclose(plik);
          break;
    case '8':
          printf("Podaj plik do wyswietlenia\n");
          scanf("%s", &nazwa);
          wyswietl(nazwa);
          break;
    case '9':
          return 1;
}
}
}
