#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

#include "naglowek.h"
#include "struktury2.h"
#define W_OK 0                   /* wartosc oznaczajaca brak bledow */
#define B_NIEPOPRAWNAOPCJA -1    /* kody bledow rozpoznawania opcji */
#define B_BRAKNAZWY   -2
#define B_BRAKWARTOSCI  -3
#define B_BRAKPLIKU   -4

#define DL_LINII 1024      /* Dlugosc buforow pomocniczych */
t_obraz obraz;
w_opcje opcje;
int odczytano = 0;


/***************************************************************************************************************************************************************/
/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*MAIN*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\/*\**/
/***************************************************************************************************************************************************************/
int main(int argc, char ** argv) {

przetwarzaj_opcje(argc, argv, &opcje, &obraz);
if(opcje.w_progu<0){
  printf("Blad: nie mozna podac progu mniejszego od 0\n");
  return 0;
}
if (opcje.plik_we != NULL){
    odczytano = czytaj(opcje.plik_we, &obraz);
    fclose(opcje.plik_we);
  }

if (opcje.negatyw==1){
    negatyw(&obraz);
}
if (opcje.progowanie==1){
    progowanie(&obraz, &opcje);
}
if (opcje.konturowanie==1){
    konturowanie(&obraz, &opcje);
}
if (opcje.wyswietlenie==1){
    wyswietl(obraz.nazwa);
    }
if(opcje.gamma==1){
    korekcjagamma(&obraz, &opcje);
}
if (opcje.rozciaganie==1){
    rozciaganie(&obraz, &opcje);
}
if (obraz.konwertowanie==1){
    konwertowanie(&obraz);
}
if (opcje.polprogczerni==1){
    polprogczern(&obraz, &opcje);
}
if (opcje.rozmywaniepionowe==1){
    rozmywaniepionowe(&obraz, &opcje);
}
    zapis(opcje.plik_wy, &obraz);
    
return 0;
}